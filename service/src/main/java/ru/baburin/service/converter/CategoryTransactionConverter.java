package ru.baburin.service.converter;

import ru.baburin.dao.model.CategoryTransactionModel;
import ru.baburin.service.model.CategoryTransaction;

public class CategoryTransactionConverter {
    public CategoryTransaction toCategoryTransaction(CategoryTransactionModel categoryTransactionModel){
        CategoryTransaction categoryTransaction = new CategoryTransaction();
        categoryTransaction.setId(categoryTransactionModel.getId());
        categoryTransaction.setName(categoryTransactionModel.getName());
        return categoryTransaction;
    }

    public CategoryTransactionModel toCategoryTransactionModel(CategoryTransaction categoryTransaction){
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel();
        categoryTransactionModel.setId(categoryTransaction.getId());
        categoryTransactionModel.setName(categoryTransaction.getName());
        return categoryTransactionModel;
    }
}
