package ru.baburin.dao.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.baburin.dao.HomeFinanceDaoException;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class InitializingOperations {

    @Autowired
    DataSource dataSource;

    private String extractSQLScriptFromFile(String filename){
        StringBuilder result = new StringBuilder("");
        try (BufferedReader inputFile = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"))) {
            String line = null;
            while (inputFile.ready()) {
                result.append(inputFile.readLine());
            }
        } catch (IOException e) {
            System.out.println("Can't open file " + filename + " for reading");
            e.printStackTrace();
        }
        return result.toString();
    }

    private void runSQLScript(String path){
        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(extractSQLScriptFromFile(path), Statement.RETURN_GENERATED_KEYS)) {
                preparedStatement.execute();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw new HomeFinanceDaoException("error execute prep" + e.getMessage(), e);
            }
        } catch (SQLException e) {
            throw new HomeFinanceDaoException("error connection", e.getCause());
        }
    }

    public void fillTables(){
        runSQLScript(getClass().getClassLoader().getResource("scripts/init_dml.sql").getPath());
    }

    public void clearTables(){
        runSQLScript(getClass().getClassLoader().getResource("scripts/clear_db.sql").getPath());
    }
}
