package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.CurrencyModel;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class CurrencyRepositoryTest {
    @Autowired
    private CurrencyRepository currencyRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getCurrencyModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        assertEquals(testData.getCurrencyModels().get(0), currencyRepository.findById(1l).get());
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(currencyRepository.findById(amountOfRows + 1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        assertEquals(testData.getCurrencyModels(), currencyRepository.findAll());
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(currencyRepository.findById(amountOfRows).isPresent());
        currencyRepository.deleteById(amountOfRows);
        assertFalse(currencyRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> currencyRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        CurrencyModel currencyModel = new CurrencyModel(null, "CND", "C", new BigDecimal("1.2"), new CurrencyModel(1l));
        assertFalse(currencyRepository.findById(amountOfRows + 1).isPresent());
        currencyRepository.save(currencyModel);
        assertEquals(currencyModel, currencyRepository.findById(currencyModel.getId()).get());
        assertEquals(amountOfRows + 1, currencyRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        CurrencyModel currencyModel = testData.getCurrencyModels().get(1);
        currencyModel.setCourseTo(new BigDecimal("6666.0"));
        currencyRepository.save(currencyModel);
        assertEquals(currencyModel, currencyRepository.findById(currencyModel.getId()).get());
        assertEquals(amountOfRows, currencyRepository.findAll().size());
    }

    @Test
    @DisplayName("Test setNewMainCurrency if currency exists in DB")
    public void setNewMainCurrency() {
        CurrencyModel newCurrency = testData.getCurrencyModels().get(2);
        currencyRepository.setMainCurrency(newCurrency, newCurrency.getCourseTo());
        List<CurrencyModel> currencyModels = testData.getCurrencyModels();
        currencyModels.stream().forEach(
                model -> {
                    model.setCourseTo(model.getCourseTo().divide(newCurrency.getCourseTo()));
                    model.setCurrencyTo(newCurrency);
                });
        assertEquals(currencyRepository.findAll(), currencyModels);
    }
}


