SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE transaction_tbl;
TRUNCATE TABLE category_transaction_tbl;
TRUNCATE TABLE currency_change_history_tbl;
TRUNCATE TABLE account_tbl;
TRUNCATE TABLE currency_tbl;
TRUNCATE TABLE account_type_tbl;
TRUNCATE TABLE user_tbl;
SET FOREIGN_KEY_CHECKS=1;