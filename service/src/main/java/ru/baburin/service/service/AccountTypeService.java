package ru.baburin.service.service;

import ru.baburin.service.model.AccountType;

import java.util.List;

public interface AccountTypeService {
    void save(AccountType model);
    AccountType findById(Long id);
    List<AccountType> findAll();
    void deleteById(Long id);
}
