package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.AccountModel;
import ru.baburin.dao.repository.AccountRepository;
import ru.baburin.service.converter.AccountConverter;
import ru.baburin.service.model.Account;
import ru.baburin.service.service.AccountService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;
    private AccountConverter accountConverter = new AccountConverter();

    @Override
    public void save(Account model) {
        model.setRegistrationDate(LocalDate.now());
        accountRepository.save(accountConverter.toAccountModel(model));
    }

    @Override
    public Account findById(Long id) {
        return accountConverter.toAccount(accountRepository.findById(id).get());
    }

    @Override
    public List<Account> findAll() {
        List<Account> accounts = new ArrayList<>();
        for (AccountModel model : accountRepository.findAll()) {
            accounts.add(accountConverter.toAccount(model));
        }
        return accounts;
    }

    @Override
    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }
}
