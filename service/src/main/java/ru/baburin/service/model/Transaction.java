package ru.baburin.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    private Long id;
    private Account sender;
    private Account receiver;
    private LocalDate transactionDate;
    private BigDecimal amount;
    private CategoryTransaction transactionCategory;
}
