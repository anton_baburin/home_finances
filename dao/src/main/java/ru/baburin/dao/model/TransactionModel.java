package ru.baburin.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "transaction_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private AccountModel sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private AccountModel receiver;

    @Column(name = "transaction_date")
    private LocalDate transactionDate;

    private BigDecimal amount;

    @ManyToOne
    @JoinColumn(name = "category_transaction_id")
    private CategoryTransactionModel transactionCategory;
}
