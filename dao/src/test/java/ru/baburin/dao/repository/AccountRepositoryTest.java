package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.AccountModel;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class AccountRepositoryTest {
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getAccountModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        AccountModel expected = testData.getAccountModels().get(0);
        AccountModel actual = accountRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(accountRepository.findById(amountOfRows + 1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<AccountModel> expected = testData.getAccountModels();
        List<AccountModel> actual = accountRepository.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(accountRepository.findById(amountOfRows).isPresent());
        accountRepository.deleteById(amountOfRows);
        assertFalse(accountRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> accountRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        AccountModel accountModel = new AccountModel(null, testData.getUserModels().get(0), "extra", new BigDecimal("100.0"),
                LocalDate.of(2018,10,10),
                testData.getAccountTypeModels().get(0), testData.getCurrencyModels().get(0));
        assertFalse(accountRepository.findById(amountOfRows+1).isPresent());
        accountRepository.save(accountModel);
        assertTrue(accountRepository.findById(accountModel.getId()).isPresent());
        assertEquals(amountOfRows + 1, accountRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        AccountModel accountModel = testData.getAccountModels().get(1);
        accountModel.setCurrency(testData.getCurrencyModels().get(0));
        assertNotEquals(accountModel, accountRepository.findById(accountModel.getId()).get());
        accountRepository.save(accountModel);
        assertEquals(accountModel, accountRepository.findById(accountModel.getId()).get());
        assertEquals(amountOfRows, accountRepository.findAll().size());
    }

  /*  @Test
    public void selectTransactionSpendingsBySenderTest(){
        System.out.println(accountRepository.selectTransactionSpendingsBySender(1l));
    }*/

}
