package ru.baburin.service.service;


import ru.baburin.service.model.Account;

import java.util.List;

public interface AccountService {
    void save(Account account);
    Account findById(Long id);
    List<Account> findAll();
    void deleteById(Long id);
}
