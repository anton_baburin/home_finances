package ru.baburin.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account{
    private Long id;
    private String name;
    private BigDecimal amount;
    private LocalDate registrationDate;
    private AccountType type;
    private Currency currency;
}
