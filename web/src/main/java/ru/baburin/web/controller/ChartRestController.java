package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.baburin.service.model.Currency;
import ru.baburin.service.model.CurrencyChangeHistory;
import ru.baburin.service.service.CurrencyChangeHistoryService;
import ru.baburin.service.service.CurrencyService;
import ru.baburin.web.ChartData;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("extractData")
public class ChartRestController {

    @Autowired
    CurrencyService currencyService;

    @Autowired
    private CurrencyChangeHistoryService currencyChangeHistoryService;

    //	@RequestMapping("/changeHistoryDataWeek")
//	public List<ChartData> extractCurrencyChangeHistories(){
    @GetMapping(value = "/changeHistoryDataWeek")
    public List<ChartData> extractCurrencyChangeHistories(@RequestParam("id") Long id) {


        List<Float> dataset = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        LocalDate day = LocalDate.now().minusWeeks(1);
        Currency currency = currencyService.findById(id);
        List<CurrencyChangeHistory> changeHistory =
                currencyChangeHistoryService.findByCurrencyAndDate(currency, day, day.plusWeeks(1));
        changeHistory = changeHistory.stream().sorted(Comparator.comparing(CurrencyChangeHistory::getChangeDate)).collect(Collectors.toList());
        BigDecimal lastCourse = currency.getCourseTo();
        for (int i = 0, j = 0; i < 7; day = day.plusDays(1), i++) {
            if (i % 2 == 1){
                StringBuilder tempDay = new StringBuilder();
                tempDay.append(day.getDayOfMonth()).append(" ").append(day.getMonth());
                labels.add(tempDay.toString());
            } else {
                labels.add("");
            }

            if  (j < changeHistory.size() && changeHistory.get(j).getChangeDate().compareTo(day) == 0) {
                lastCourse = changeHistory.get(j).getCourseTo();
                j++;
            }
            dataset.add(lastCourse.floatValue());

        }

        List<ChartData> chartData = new ArrayList<>();
        chartData.add(new ChartData(labels, dataset));
        return chartData;
    }

    @GetMapping(value = "/accountsData")
    public List<ChartData> extractAccounts(@RequestParam("id") Long id) {
        List<Float> dataset = new ArrayList<>();
        List<String> labels = new ArrayList<>();

        labels.add("category 1");
        labels.add("category 2");
        dataset.add(20f);
        dataset.add(35f);

        List<ChartData> chartData = new ArrayList<>();
        chartData.add(new ChartData(labels, dataset));
        return chartData;
    }
}
