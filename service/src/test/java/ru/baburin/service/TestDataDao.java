package ru.baburin.service;

import ru.baburin.dao.model.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TestDataDao {
    private List<UserModel> userModels = new ArrayList<>();
    private List<AccountTypeModel> accountTypeModels = new ArrayList<>();
    private List<CurrencyModel> currencyModels = new ArrayList<>();
    private List<AccountModel> accountModels = new ArrayList<>();
    private List<CurrencyChangeHistoryModel> currencyChangeHistoryModels = new ArrayList<>();
    private List<CategoryTransactionModel> categoryTransactionModels = new ArrayList<>();
    private List<TransactionModel> transactionModels = new ArrayList<>();

    public TestDataDao() {
        fillUserModels();
        fillAccountTypeModels();
        fillCurrencyModels();
        fillAccountModels();
        fillCurrencyChangeHistoryModels();
        fillCategoryTransactionModels();
        fillTransactionModels();
    }

    private void fillUserModels(){
        userModels.add(new UserModel(1l, "username 1", "12345678", null, Role.ADMIN));
        userModels.add(new UserModel(2l, "username 2", "12345678", null, Role.USER));
        userModels.add(new UserModel(3l, "username 3", "12345678", null, Role.USER));
    }

    private void fillAccountTypeModels(){
        accountTypeModels.add(new AccountTypeModel(1l, "Cash"));
        accountTypeModels.add(new AccountTypeModel(2l, "Debit"));
        accountTypeModels.add(new AccountTypeModel(3l, "Deposit"));
    }

    private void fillCurrencyModels(){
        currencyModels.add(new CurrencyModel(1l, "USD", "$", new BigDecimal("1.0"), new CurrencyModel(1l)));
        currencyModels.add(new CurrencyModel(2l, "RUB", "R", new BigDecimal("60.0"), new CurrencyModel(1l)));
        currencyModels.add(new CurrencyModel(3l, "EUR", "E", new BigDecimal("0.8"), new CurrencyModel(1l)));

    }

    private void fillAccountModels(){
        accountModels.add(new AccountModel(1l, userModels.get(0), "Main", new BigDecimal("50000.0"),
                LocalDate.of(2018,10,10), accountTypeModels.get(1), currencyModels.get(0)));
        accountModels.add(new AccountModel(2l, userModels.get(0), "Emergency", new BigDecimal("60000.0"),
                LocalDate.of(2019,1,1), accountTypeModels.get(0), currencyModels.get(1)));
        accountModels.add(new AccountModel(3l, userModels.get(1), "Salary card", new BigDecimal("100000.0"),
                LocalDate.of(2017,2,1), accountTypeModels.get(0), currencyModels.get(0)));
    }

    private void fillCurrencyChangeHistoryModels(){
        currencyChangeHistoryModels.add(new CurrencyChangeHistoryModel(1l, currencyModels.get(1), LocalDate.of(2018,12,12), new BigDecimal("58.0"), currencyModels.get(0)));
        currencyChangeHistoryModels.add(new CurrencyChangeHistoryModel(2l, currencyModels.get(1), LocalDate.of(2019,1,1), new BigDecimal("62.0"), currencyModels.get(0)));
        currencyChangeHistoryModels.add(new CurrencyChangeHistoryModel(3l, currencyModels.get(1), LocalDate.of(2017,2,3), new BigDecimal("0.9"), currencyModels.get(0)));
    }

    private void fillCategoryTransactionModels(){
        categoryTransactionModels.add(new CategoryTransactionModel(1l, "category 1"));
        categoryTransactionModels.add(new CategoryTransactionModel(2l, "category 2"));
        categoryTransactionModels.add(new CategoryTransactionModel(3l, "category 3"));
    }

    private void fillTransactionModels(){
        transactionModels.add(new TransactionModel(1l, accountModels.get(0), accountModels.get(1),
                LocalDate.of(2018, 10, 10), new BigDecimal("300.0"), categoryTransactionModels.get(0)));
        transactionModels.add(new TransactionModel(2l, accountModels.get(0), accountModels.get(1),
                LocalDate.of(2017, 9, 10), new BigDecimal("600.0"), categoryTransactionModels.get(0)));
        transactionModels.add(new TransactionModel(3l, accountModels.get(1), accountModels.get(0),
                LocalDate.of(2015, 10, 3), new BigDecimal("400.0"), categoryTransactionModels.get(1)));
    }

    public List<UserModel> getUserModels() {
        return userModels;
    }

    public List<TransactionModel> getTransactionModels() {
        return transactionModels;
    }

    public List<AccountTypeModel> getAccountTypeModels() {
        return accountTypeModels;
    }

    public List<CurrencyModel> getCurrencyModels() {
        return currencyModels;
    }

    public List<AccountModel> getAccountModels() {
        return accountModels;
    }

    public List<CurrencyChangeHistoryModel> getCurrencyChangeHistoryModels() {
        return currencyChangeHistoryModels;
    }

    public List<CategoryTransactionModel> getCategoryTransactionModels() {
        return categoryTransactionModels;
    }
}
