package ru.baburin.service.service;


import ru.baburin.service.model.Currency;
import ru.baburin.service.model.CurrencyChangeHistory;

import java.time.LocalDate;
import java.util.List;

public interface CurrencyChangeHistoryService {
    void save(CurrencyChangeHistory model);
    CurrencyChangeHistory findById(Long id);
    List<CurrencyChangeHistory> findAll();
    void deleteById(Long id);
    List<CurrencyChangeHistory> findByCurrencyAndDate(Currency currency, LocalDate dateFrom, LocalDate dateTo);
}
