package ru.baburin.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "account_type_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountTypeModel {
    //CASH, DEBIT_CARD, CREDIT_CARD, DEPOSIT;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_type_id")
    private Long id;

    private String name;
}