package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.model.CurrencyChangeHistoryModel;
import ru.baburin.dao.repository.CurrencyChangeHistoryRepository;
import ru.baburin.dao.repository.CurrencyRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.CurrencyChangeHistory;
import ru.baburin.service.service.CurrencyChangeHistoryService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class CurrencyChangeHistoryServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private CurrencyChangeHistoryRepository currencyChangeHistoryRepository;

    @Mock
    private CurrencyRepository currencyRepository;

    @Autowired
    @InjectMocks
    private CurrencyChangeHistoryService currencyChangeHistoryService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(currencyChangeHistoryRepository.findAll()).thenReturn(testDataDao.getCurrencyChangeHistoryModels());
        List<CurrencyChangeHistory> actual = testDataService.getCurrencyChangeHistories();
        List<CurrencyChangeHistory> expected = currencyChangeHistoryService.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(currencyChangeHistoryRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getCurrencyChangeHistoryModels().get(0)));
        CurrencyChangeHistory actual = testDataService.getCurrencyChangeHistories().get(0);
        CurrencyChangeHistory expected = currencyChangeHistoryService.findById(1l);
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        CurrencyChangeHistory currencyChangeHistory = testDataService.getCurrencyChangeHistories().get(0);
        currencyChangeHistory.setId(null);
        when(currencyRepository.findById(anyLong())).thenReturn(
                Optional.of(testDataDao.getCurrencyModels().get(1)));
        currencyChangeHistoryService.save(currencyChangeHistory);
        verify(currencyChangeHistoryRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        CurrencyChangeHistory currencyChangeHistory = testDataService.getCurrencyChangeHistories().get(0);
        currencyChangeHistory.setCourseTo(new BigDecimal("111.0"));
        when(currencyRepository.findById(anyLong())).thenReturn(
                Optional.of(testDataDao.getCurrencyModels().get(1)));
        currencyChangeHistoryService.save(currencyChangeHistory);
        verify(currencyChangeHistoryRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        currencyChangeHistoryService.deleteById(3l);
        verify(currencyChangeHistoryRepository).deleteById(anyLong());
    }

    @Test
    @Description("Test findByDateAndCurrency")
    public void findByIdAndCurrencyTest(){
        List<CurrencyChangeHistoryModel> returnOfTheMock = new ArrayList<>();
        returnOfTheMock.add(testDataDao.getCurrencyChangeHistoryModels().get(0));
        returnOfTheMock.add(testDataDao.getCurrencyChangeHistoryModels().get(1));
        LocalDate dateFrom = LocalDate.of(2018,10,10);
        LocalDate dateTo = LocalDate.of(2019,1,1);
        when(currencyChangeHistoryRepository.findByCurrencyAndChangeDateBetween(
                testDataDao.getCurrencyModels().get(1), dateFrom, dateTo)).
                thenReturn(returnOfTheMock);

        List<CurrencyChangeHistory> expected = new ArrayList<>();
        expected.add(testDataService.getCurrencyChangeHistories().get(0));
        expected.add(testDataService.getCurrencyChangeHistories().get(1));
        List<CurrencyChangeHistory> actual = currencyChangeHistoryService.findByCurrencyAndDate(
                testDataService.getCurrencies().get(1), dateFrom, dateTo);

        assertEquals(expected, actual);
    }
}
