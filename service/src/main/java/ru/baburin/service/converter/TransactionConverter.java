package ru.baburin.service.converter;

import ru.baburin.dao.model.TransactionModel;
import ru.baburin.service.model.Transaction;

public class TransactionConverter {
    public Transaction toTransaction(TransactionModel transactionModel){
        Transaction transaction = new Transaction();
        transaction.setId(transactionModel.getId());
        transaction.setAmount(transactionModel.getAmount());
        AccountConverter accountConverter = new AccountConverter();
        transaction.setReceiver(accountConverter.toAccount(transactionModel.getReceiver()));
        transaction.setSender(accountConverter.toAccount(transactionModel.getSender()));
        transaction.setTransactionDate(transactionModel.getTransactionDate());
        transaction.setTransactionCategory(new CategoryTransactionConverter().toCategoryTransaction(transactionModel.getTransactionCategory()));
        return transaction;
    }

    public TransactionModel toTransactionModel(Transaction transaction){
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setId(transaction.getId());
        transactionModel.setAmount(transaction.getAmount());
        AccountConverter accountConverter = new AccountConverter();
        transactionModel.setReceiver(accountConverter.toAccountModel(transaction.getReceiver()));
        transactionModel.setSender(accountConverter.toAccountModel(transaction.getSender()));
        transactionModel.setTransactionDate(transaction.getTransactionDate());
        transactionModel.setTransactionCategory(new CategoryTransactionConverter().toCategoryTransactionModel(transaction.getTransactionCategory()));
        return transactionModel;
    }
}
