CREATE TABLE IF NOT EXISTS user_tbl(
    user_id     INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
    username    VARCHAR(255)                    NOT NULL ,
    password    VARCHAR(255)                    NOT NULL ,
    role        VARCHAR(255)                    NOT NULL
);


CREATE TABLE IF NOT EXISTS account_type_tbl(
    account_type_id INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
    name            VARCHAR(50)                     NOT NULL
);


CREATE TABLE IF NOT EXISTS currency_tbl(
                                           currency_id     INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
                                           name            VARCHAR(50)                     NOT NULL ,
                                           symbol          VARCHAR(10)                     NOT NULL ,
                                           course_to       DOUBLE                          NOT NULL ,
                                           currency_to_id  INT                             NULL ,
                                           FOREIGN KEY (currency_to_id) REFERENCES currency_tbl (currency_id)
);

CREATE TABLE IF NOT EXISTS account_tbl(
                                          account_id        INT AUTO_INCREMENT PRIMARY KEY    NOT NULL ,
                                          user_id           INT                               NOT NULL ,
                                          name              VARCHAR(50)                       NOT NULL ,
                                          amount            DOUBLE                            NOT NULL ,
                                          registration_date DATETIME                          NOT NULL ,
                                          account_type_id   INT                               NOT NULL ,
                                          currency_id       INT                               NOT NULL ,
                                          FOREIGN KEY (account_type_id) REFERENCES account_type_tbl (account_type_id),
                                          FOREIGN KEY (currency_id) REFERENCES currency_tbl (currency_id),
                                          FOREIGN KEY (user_id) REFERENCES user_tbl (user_id)
);


CREATE TABLE IF NOT EXISTS currency_change_history_tbl(
                                                          currency_change_history_id  INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
                                                          currency_id                 INT                             NOT NULL ,
                                                          change_date                 DATETIME                        NOT NULL ,
                                                          course_to                   DOUBLE                          NOT NULL ,
                                                          currency_to_id             INT                             NOT NULL ,
                                                          FOREIGN KEY (currency_id) REFERENCES currency_tbl(currency_id),
                                                          FOREIGN KEY (currency_to_id) REFERENCES currency_tbl(currency_id)
);


CREATE TABLE IF NOT EXISTS category_transaction_tbl(
                                                       category_transaction_id INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
                                                       name                    VARCHAR(50)                     NOT NULL
);

CREATE TABLE IF NOT EXISTS transaction_tbl(
                                              transaction_id          INT AUTO_INCREMENT PRIMARY KEY  NOT NULL ,
                                              sender_id               INT                             NULL ,
                                              receiver_id             INT                             NULL ,
                                              transaction_date        DATETIME                        NOT NULL ,
                                              amount                  DOUBLE                          NOT NULL ,
                                              category_transaction_id INT                             NOT NULL ,
                                              FOREIGN KEY (sender_id) REFERENCES account_tbl (account_id),
                                              FOREIGN KEY (receiver_id) REFERENCES account_tbl (account_id),
                                              FOREIGN KEY (category_transaction_id) REFERENCES category_transaction_tbl (category_transaction_id)
);
