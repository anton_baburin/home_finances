package ru.baburin.service.service;

import ru.baburin.service.model.CategoryTransaction;

import java.util.List;

public interface CategoryTransactionService {
    void save(CategoryTransaction model);
    CategoryTransaction findById(Long id);
    List<CategoryTransaction> findAll();
    void deleteById(Long id);
}
