package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.TransactionModel;
import ru.baburin.dao.repository.TransactionRepository;
import ru.baburin.service.converter.TransactionConverter;
import ru.baburin.service.model.Transaction;
import ru.baburin.service.service.TransactionService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;
    private TransactionConverter transactionConverter = new TransactionConverter();

    @Override
    public void save(Transaction model) {
        model.setTransactionDate(LocalDate.now());
        transactionRepository.save(transactionConverter.toTransactionModel(model));
    }

    @Override
    public Transaction findById(Long id) {
        return transactionConverter.toTransaction(transactionRepository.findById(id).get());
    }

    @Override
    public List<Transaction> findAll() {
        List<Transaction> transactions = new ArrayList<>();
        for (TransactionModel model : transactionRepository.findAll()) {
            transactions.add(transactionConverter.toTransaction(model));
        }
        return transactions;
    }

    @Override
    public void deleteById(Long id) {
        transactionRepository.deleteById(id);
    }
}
