package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.AccountTypeRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.AccountType;
import ru.baburin.service.service.AccountTypeService;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class AccountTypeServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private AccountTypeRepository accountTypeRepository;

    @Autowired
    @InjectMocks
    private AccountTypeService accountTypeService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(accountTypeRepository.findAll()).thenReturn(testDataDao.getAccountTypeModels());
        List<AccountType> actual = testDataService.getAccountTypes();
        List<AccountType> expected = accountTypeService.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(accountTypeRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getAccountTypeModels().get(0)));
        AccountType actual = testDataService.getAccountTypes().get(0);
        AccountType expected = accountTypeService.findById(1l);
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        AccountType accountType = testDataService.getAccountTypes().get(0);
        accountType.setId(null);
        accountTypeService.save(accountType);
        verify(accountTypeRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        AccountType accountType = testDataService.getAccountTypes().get(0);
        accountType.setName("new name");
        accountTypeService.save(accountType);
        verify(accountTypeRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        accountTypeService.deleteById(3l);
        verify(accountTypeRepository).deleteById(anyLong());
    }
}
