package ru.baburin.dao.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ru.baburin.dao.model.CurrencyModel;

import java.math.BigDecimal;

public interface CurrencyRepository extends JpaRepository<CurrencyModel, Long> {
    @Transactional
    @Modifying
    @Query("UPDATE CurrencyModel currency SET currency.currencyTo = :model , currency.courseTo = currency.courseTo/(:courseTo)")
    int setMainCurrency(@Param("model") CurrencyModel currencyModel, @Param("courseTo") BigDecimal courseTo);
}
