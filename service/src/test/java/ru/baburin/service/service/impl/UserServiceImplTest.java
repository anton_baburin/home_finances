package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.UserRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.User;
import ru.baburin.service.service.UserService;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class UserServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private UserRepository userRepository;

    @Autowired
    @InjectMocks
    private UserService userService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(userRepository.findAll()).thenReturn(testDataDao.getUserModels());
        List<User> actual = testDataService.getUsers();
        List<User> expected = userService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(userRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getUserModels().get(0)));
        User actual = testDataService.getUsers().get(0);
        User expected = userService.findById(1l);

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        User user = testDataService.getUsers().get(0);
        user.setId(null);

        userService.save(user);
        verify(userRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        User user = testDataService.getUsers().get(0);
        user.setUsername("new username");
        userService.save(user);
        verify(userRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        userService.deleteById(3l);
        verify(userRepository).deleteById(anyLong());
    }
}
