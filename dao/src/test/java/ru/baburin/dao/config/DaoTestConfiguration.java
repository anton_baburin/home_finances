package ru.baburin.dao.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.baburin.dao.repository.InitializingOperations;

@Configuration
@Import(DaoConfiguration.class)
@ComponentScan("ru.baburin.dao")
public class DaoTestConfiguration {
    @Bean
    InitializingOperations initializingOperations(){
        return new InitializingOperations();
    }
}
