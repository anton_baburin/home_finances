package ru.baburin.service;

import ru.baburin.dao.model.Role;
import ru.baburin.service.model.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TestDataService {
    private List<User> users = new ArrayList<>();
    private List<AccountType> accountTypes = new ArrayList<>();
    private List<Currency> currencies = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<CurrencyChangeHistory> currencyChangeHistories = new ArrayList<>();
    private List<CategoryTransaction> categoryTransactions = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    public TestDataService() {
        fillUsers();
        fillAccountTypes();
        fillCurrencies();
        fillAccounts();
        fillCurrencyChangeHistories();
        fillCategoryTransactions();
        fillTransactions();
    }

    private void fillUsers(){
        users.add(new User(1l, "username 1", "12345678", null, Role.ADMIN));
        users.add(new User(2l, "username 2", "12345678", null, Role.USER));
        users.add(new User(3l, "username 3", "12345678", null, Role.USER));
    }

    private void fillAccountTypes(){
        accountTypes.add(new AccountType(1l, "Cash"));
        accountTypes.add(new AccountType(2l, "Debit"));
        accountTypes.add(new AccountType(3l, "Deposit"));
    }

    private void fillCurrencies(){
        currencies.add(new Currency(1l, "USD", "$", new BigDecimal("1.0"), new Currency(1l)));
        currencies.add(new Currency(2l, "RUB", "R", new BigDecimal("60.0"), new Currency(1l)));
        currencies.add(new Currency(3l, "EUR", "E", new BigDecimal("0.8"), new Currency(1l)));
    }

    private void fillAccounts(){
        accounts.add(new Account(1l, "Main", new BigDecimal("50000.0"),
                LocalDate.of(2018,10,10), accountTypes.get(1), currencies.get(0)));
        accounts.add(new Account(2l, "Emergency", new BigDecimal("60000.0"),
                LocalDate.of(2019,1,1), accountTypes.get(0), currencies.get(1)));
        accounts.add(new Account(3l, "Salary card", new BigDecimal("100000.0"),
                LocalDate.of(2017,2,1), accountTypes.get(0), currencies.get(0)));
    }

    private void fillCurrencyChangeHistories(){
        currencyChangeHistories.add(new CurrencyChangeHistory(1l, currencies.get(1), LocalDate.of(2018,12,12), new BigDecimal("58.0"), currencies.get(0)));
        currencyChangeHistories.add(new CurrencyChangeHistory(2l, currencies.get(1), LocalDate.of(2019,1,1), new BigDecimal("62.0"), currencies.get(0)));
        currencyChangeHistories.add(new CurrencyChangeHistory(3l, currencies.get(1), LocalDate.of(2017,2,3), new BigDecimal("0.9"), currencies.get(0)));
    }

    private void fillCategoryTransactions(){
        categoryTransactions.add(new CategoryTransaction(1l, "category 1"));
        categoryTransactions.add(new CategoryTransaction(2l, "category 2"));
        categoryTransactions.add(new CategoryTransaction(3l, "category 3"));
    }

    private void fillTransactions(){
        transactions.add(new Transaction(1l, accounts.get(0), accounts.get(1),
                LocalDate.of(2018, 10, 10), new BigDecimal("300.0"), categoryTransactions.get(0)));
        transactions.add(new Transaction(2l, accounts.get(0), accounts.get(1),
                LocalDate.of(2017, 9, 10), new BigDecimal("600.0"), categoryTransactions.get(0)));
        transactions.add(new Transaction(3l, accounts.get(1), accounts.get(0),
                LocalDate.of(2015, 10, 3), new BigDecimal("400.0"), categoryTransactions.get(1)));

    }

    public List<User> getUsers() {
        return users;
    }
    public List<AccountType> getAccountTypes() {
        return accountTypes;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public List<CurrencyChangeHistory> getCurrencyChangeHistories() {
        return currencyChangeHistories;
    }

    public List<CategoryTransaction> getCategoryTransactions() {
        return categoryTransactions;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }
}
