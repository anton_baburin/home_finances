package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.CurrencyModel;
import ru.baburin.dao.repository.CurrencyRepository;
import ru.baburin.service.converter.CurrencyConverter;
import ru.baburin.service.model.Currency;
import ru.baburin.service.service.CurrencyService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyRepository currencyRepository;
    private CurrencyConverter currencyConverter = new CurrencyConverter();

    @Override
    public void save(Currency model) {
        List<Currency> currencies = findAll();
        if (currencies.size() == 0) {
            model.setCurrencyTo(model);
        } else {
            model.setCurrencyTo(currencies.get(0).getCurrencyTo());
        }
        currencyRepository.save(currencyConverter.toCurrencyModel(model));
    }

    @Override
    public Currency findById(Long id) {
        return currencyConverter.toCurrency(currencyRepository.findById(id).get());
    }

    @Override
    public List<Currency> findAll() {
        List<Currency> currencies = new ArrayList<>();
        for (CurrencyModel model : currencyRepository.findAll()) {
            currencies.add(currencyConverter.toCurrency(model));
        }
        return currencies;
    }

    @Override
    public void deleteById(Long id) {
        currencyRepository.deleteById(id);
    }

    @Override
    public void setMainCurrency(Currency model) {
        currencyRepository.setMainCurrency(currencyConverter.toCurrencyModel(model), model.getCourseTo());
    }
}
