package ru.baburin.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "currency_change_history_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyChangeHistoryModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_change_history_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyModel currency;

    @Column(name = "change_date")
    private LocalDate changeDate;

    @Column(name = "course_to")
    private BigDecimal courseTo;

    @ManyToOne
    @JoinColumn(name = "currency_to_id")
    private CurrencyModel currencyTo;
}
