package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.TransactionModel;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class TransactionRepositoryTest {
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getAccountModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        TransactionModel expected = testData.getTransactionModels().get(0);
        TransactionModel actual = transactionRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(transactionRepository.findById(amountOfRows + 1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<TransactionModel> expected = testData.getTransactionModels();
        List<TransactionModel> actual = transactionRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(transactionRepository.findById(amountOfRows).isPresent());
        transactionRepository.deleteById(amountOfRows);
        assertFalse(transactionRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> transactionRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        TransactionModel transactionModel = testData.getTransactionModels().get(1);
        transactionModel.setId(null);
        BigDecimal amount = new BigDecimal("1314.0");
        transactionModel.setAmount(amount);
        assertFalse(transactionRepository.findById(amountOfRows+1).isPresent());
        transactionRepository.save(transactionModel);
        assertTrue(transactionRepository.findById(transactionModel.getId()).isPresent());
        assertEquals(amountOfRows + 1, transactionRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        TransactionModel transactionModel = testData.getTransactionModels().get(1);
        transactionModel.setAmount(new BigDecimal("666.0"));
        assertNotEquals(transactionModel, transactionRepository.findById(transactionModel.getId()).get());
        transactionRepository.save(transactionModel);
        assertEquals(transactionModel, transactionRepository.findById(transactionModel.getId()).get());
        assertEquals(amountOfRows, transactionRepository.findAll().size());
    }
}
