package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.Account;
import ru.baburin.service.model.CategoryTransaction;
import ru.baburin.service.model.Currency;
import ru.baburin.service.model.Transaction;
import ru.baburin.service.service.AccountService;
import ru.baburin.service.service.CategoryTransactionService;
import ru.baburin.service.service.CurrencyService;
import ru.baburin.service.service.TransactionService;

import java.util.List;

//Todo сделать правильный перевод денег по транзакции
//Todo добавить currency в транзакцию
//Todo пазобраться с аяксом
@Controller
@RequestMapping("transaction")
public class TransactionController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private CategoryTransactionService categoryTransactionService;
    @Autowired
    private TransactionService transactionService;

    @GetMapping("/find_all")
    public String findAll(Model model){
        List<Transaction> transactions = transactionService.findAll();
        model.addAttribute("transactions", transactions);
        return "transaction/find_all";
    }

    @GetMapping("/fill_save_form")
    public String fillSaveForm(Model model){
        Transaction transaction = new Transaction();
        model.addAttribute("transaction", transaction);
        return "transaction/transaction_form";
    }

    @GetMapping("/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        Transaction transaction = transactionService.findById(id);
        model.addAttribute("transaction", transaction);
        return "transaction/transaction_form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("account") Transaction transaction) {
    //    transaction.setCurrency(currencyService.findById(transaction.get().getId()));
        transaction.setSender(accountService.findById(transaction.getSender().getId()));
        transaction.setReceiver(accountService.findById(transaction.getReceiver().getId()));
        transactionService.save(transaction);
        return "redirect:/transaction/find_all";
    }

    @GetMapping(value="/delete")
    public String remove(@RequestParam("id") Long id) {
        transactionService.deleteById(Long.valueOf(id));
        return "redirect:/transaction/find_all";
    }

    @ModelAttribute("accounts")
    public List<Account> getAccounts(){
        return accountService.findAll();
    }

    @ModelAttribute("currencies")
    public List<Currency> getCurrencies(){
        return currencyService.findAll();
    }

    @ModelAttribute("categoryTransactions")
    public List<CategoryTransaction> getCategoryTransactions(){
        return categoryTransactionService.findAll();
    }
}
