package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.CurrencyChangeHistoryModel;
import ru.baburin.dao.model.CurrencyModel;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class CurrencyChangeHistoryRepositoryTest {
    @Autowired
    private CurrencyChangeHistoryRepository currencyChangeHistoryRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getCurrencyChangeHistoryModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        CurrencyChangeHistoryModel expected = testData.getCurrencyChangeHistoryModels().get(0);
        CurrencyChangeHistoryModel actual = currencyChangeHistoryRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(currencyChangeHistoryRepository.findById(amountOfRows + 1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<CurrencyChangeHistoryModel> expected = testData.getCurrencyChangeHistoryModels();
        List<CurrencyChangeHistoryModel> actual = currencyChangeHistoryRepository.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(currencyChangeHistoryRepository.findById(amountOfRows).isPresent());
        currencyChangeHistoryRepository.deleteById(amountOfRows);
        assertFalse(currencyChangeHistoryRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> currencyChangeHistoryRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        CurrencyChangeHistoryModel currencyChangeHistoryModel = new CurrencyChangeHistoryModel(null, testData.getCurrencyModels().get(2), LocalDate.of(2017,12,12), new BigDecimal("58.0"),
                new CurrencyModel(1l, "USD", "$", new BigDecimal("1.0"), testData.getCurrencyModels().get(0)));
        assertFalse(currencyChangeHistoryRepository.findById(amountOfRows + 1).isPresent());
        currencyChangeHistoryRepository.save(currencyChangeHistoryModel);
        assertTrue(currencyChangeHistoryRepository.findById(currencyChangeHistoryModel.getId()).isPresent());
        assertEquals(amountOfRows + 1, currencyChangeHistoryRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        CurrencyChangeHistoryModel currencyChangeHistoryModel = new CurrencyChangeHistoryModel(2l, testData.getCurrencyModels().get(2), LocalDate.of(2017,12,12), new BigDecimal("58.0"),
                new CurrencyModel(1l, "USD", "$", new BigDecimal("1.0"), testData.getCurrencyModels().get(0)));
        assertNotEquals(currencyChangeHistoryModel, currencyChangeHistoryRepository.findById(currencyChangeHistoryModel.getId()).get());
        currencyChangeHistoryRepository.save(currencyChangeHistoryModel);
        assertEquals(currencyChangeHistoryModel, currencyChangeHistoryRepository.findById(currencyChangeHistoryModel.getId()).get());
        assertEquals(amountOfRows, currencyChangeHistoryRepository.findAll().size());
    }

    @Test
    @DisplayName("Test findByCurrencyAndChangeDate")
    public void findByCurrencyAndChangeDateTest() {
        List<CurrencyChangeHistoryModel> expected = new ArrayList<>();
        expected.add(testData.getCurrencyChangeHistoryModels().get(0));
        expected.add(testData.getCurrencyChangeHistoryModels().get(1));
        LocalDate dateFrom = LocalDate.of(2018,10,10);
        LocalDate dateTo = LocalDate.of(2019,1,1);
        List<CurrencyChangeHistoryModel> actual = currencyChangeHistoryRepository.findByCurrencyAndChangeDateBetween(
                testData.getCurrencyModels().get(1), dateFrom, dateTo);
        assertEquals(expected, actual);
    }
}
