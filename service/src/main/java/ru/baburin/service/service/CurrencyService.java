package ru.baburin.service.service;


import ru.baburin.service.model.Currency;

import java.util.List;

public interface CurrencyService {
    void save(Currency model);
    Currency findById(Long id);
    List<Currency> findAll();
    void deleteById(Long id);
    void setMainCurrency(Currency model);
}
