package ru.baburin.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.AccountModel;

public interface AccountRepository extends JpaRepository<AccountModel, Long> {
  /*  @Transactional
    @Modifying
    @Query("SELECT category.name as categoryName, SUM(transaction.amount) as sumOfSpendings " +
            "FROM CategoryTransactionModel category LEFT JOIN TransactionModel transaction " +
            "ON category.id = transaction.transactionCategory.id " +
            "WHERE transaction.sender = :id " +
            "GROUP BY category.id")
    CategoryTransactionSpendingsModel selectTransactionSpendingsBySender(@Param("id") Long accountModelId);

SELECT category.name as categoryName, SUM(transaction.amount) as sumOfSpendings
FROM category_transaction_tbl category LEFT JOIN transaction_tbl transaction
ON category.category_transaction_id = transaction.category_transaction_id
WHERE transaction.sender_id = 1
GROUP BY category.category_transaction_id;
*/
}