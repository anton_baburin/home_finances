package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.CurrencyRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.Currency;
import ru.baburin.service.service.CurrencyService;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class CurrencyServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private CurrencyRepository currencyRepository;

    @Autowired
    @InjectMocks
    private CurrencyService currencyService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(currencyRepository.findAll()).thenReturn(testDataDao.getCurrencyModels());
        List<Currency> actual = testDataService.getCurrencies();
        List<Currency> expected = currencyService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(currencyRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getCurrencyModels().get(0)));
        Currency actual = testDataService.getCurrencies().get(0);
        Currency expected = currencyService.findById(1l);

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        Currency currency = testDataService.getCurrencies().get(0);
        currency.setId(null);
        currencyService.save(currency);
        verify(currencyRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        Currency currency = testDataService.getCurrencies().get(0);
        currency.setName("new name");
        currencyService.save(currency);
        verify(currencyRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        currencyService.deleteById(3l);
        verify(currencyRepository).deleteById(anyLong());
    }

    @Test
    @Description("Test setMainCurrency")
    public void setMainCurrencyTest(){
        Currency currency = testDataService.getCurrencies().get(1);
        currencyService.setMainCurrency(currency);
    }
}
