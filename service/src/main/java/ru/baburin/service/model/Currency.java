package ru.baburin.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currency {
    private Long id;
    private String name;
    private String symbol;
    private BigDecimal courseTo;
    private Currency currencyTo;

    public Currency(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency that = (Currency) o;
        return id.equals(that.id) &&
                name.equals(that.name) &&
                symbol.equals(that.symbol) &&
                courseTo.equals(that.courseTo) &&
                currencyTo.getId().equals(that.currencyTo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, symbol, courseTo, currencyTo.getId());
    }
}
