package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.Account;
import ru.baburin.service.model.AccountType;
import ru.baburin.service.model.Currency;
import ru.baburin.service.service.AccountService;
import ru.baburin.service.service.AccountTypeService;
import ru.baburin.service.service.CurrencyService;

import java.util.List;

@Controller
@RequestMapping("account")
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private AccountTypeService accountTypeService;

    @GetMapping("/find_all")
    public String findAll(Model model){
        List<Account> accounts = accountService.findAll();
        model.addAttribute("accounts", accounts);
        return "account/find_all";
    }

    @GetMapping("/fill_save_form")
    public String fillSaveForm(Model model){
        Account account = new Account();
        model.addAttribute("account", account);
        return "account/account_form";
    }

    @GetMapping("/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        Account account = accountService.findById(id);
        model.addAttribute("account", account);
        return "account/account_form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("account") Account account) {
        account.setCurrency(currencyService.findById(account.getCurrency().getId()));
        accountService.save(account);
        return "redirect:/account/find_all";
    }

    @GetMapping(value="/delete")
    public String remove(@RequestParam("id") Long id) {
        accountService.deleteById(Long.valueOf(id));
        return "redirect:/account/find_all";
    }

    @GetMapping(value="/chart")
    public String drawPieChart(@RequestParam("id") Long id, Model model) {
        // currencyChangeHistoryService.deleteById(Long.valueOf(id));
         return "account/chart";
    }

    @ModelAttribute("accountTypes")
    public List<AccountType> getAccountTypes(){
        return accountTypeService.findAll();
    }

    @ModelAttribute("currencies")
    public List<Currency> getCurrencies(){
        return currencyService.findAll();
    }
}
