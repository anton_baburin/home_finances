package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.Role;
import ru.baburin.dao.model.UserModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getUserModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        UserModel expected = testData.getUserModels().get(0);
        UserModel actual = userRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(userRepository.findById(amountOfRows+1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<UserModel> expected = testData.getUserModels();
        List<UserModel> actual = userRepository.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(userRepository.findById(amountOfRows).isPresent());
        userRepository.deleteById(amountOfRows);
        assertFalse(userRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> userRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        UserModel userModel = new UserModel(null, "new username", "password", null                                                                                                                                                                                                                                                                                                      , Role.USER);
        assertFalse(userRepository.findById(amountOfRows + 1).isPresent());
        userRepository.save(userModel);
        assertEquals(userModel, userRepository.findById(userModel.getId()).get());
        assertEquals(amountOfRows +1, userRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        UserModel userModel = new UserModel(2l, "new username", "password", null                                                                                                                                                                                                                                                                                                      , Role.USER);
        assertFalse(userRepository.findById(amountOfRows + 1).isPresent());
        userRepository.save(userModel);
        assertEquals(userModel, userRepository.findById(userModel.getId()).get());
        assertEquals(amountOfRows, userRepository.findAll().size());
    }
}