package ru.baburin.service.converter;

import ru.baburin.dao.model.CurrencyChangeHistoryModel;
import ru.baburin.service.model.CurrencyChangeHistory;

public class CurrencyChangeHistoryConverter {
    public CurrencyChangeHistory toCurrencyChangeHistory(CurrencyChangeHistoryModel currencyChangeHistoryModel){
        CurrencyChangeHistory currencyChangeHistory = new CurrencyChangeHistory();
        currencyChangeHistory.setId(currencyChangeHistoryModel.getId());
        currencyChangeHistory.setChangeDate(currencyChangeHistoryModel.getChangeDate());
        currencyChangeHistory.setCourseTo(currencyChangeHistoryModel.getCourseTo());
        CurrencyConverter currencyConverter = new CurrencyConverter();
        currencyChangeHistory.setCurrency(currencyConverter.toCurrency(currencyChangeHistoryModel.getCurrency()));
        currencyChangeHistory.setCurrencyTo(currencyConverter.toCurrency(currencyChangeHistoryModel.getCurrencyTo()));
        return currencyChangeHistory;
    }

    public CurrencyChangeHistoryModel toCurrencyChangeHistoryModel(CurrencyChangeHistory currencyChangeHistory){
        CurrencyChangeHistoryModel currencyChangeHistoryModel = new CurrencyChangeHistoryModel();
        currencyChangeHistoryModel.setId(currencyChangeHistory.getId());
        currencyChangeHistoryModel.setChangeDate(currencyChangeHistory.getChangeDate());
        currencyChangeHistoryModel.setCourseTo(currencyChangeHistory.getCourseTo());
        CurrencyConverter currencyConverter = new CurrencyConverter();
        currencyChangeHistoryModel.setCurrency(currencyConverter.toCurrencyModel(currencyChangeHistory.getCurrency()));
        currencyChangeHistoryModel.setCurrencyTo(currencyConverter.toCurrencyModel(currencyChangeHistory.getCurrencyTo()));
        return currencyChangeHistoryModel;
    }
}
