package ru.baburin.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyChangeHistory {
    private Long id;
    private Currency currency;
    private LocalDate changeDate;
    private BigDecimal courseTo;
    private Currency currencyTo;
}
