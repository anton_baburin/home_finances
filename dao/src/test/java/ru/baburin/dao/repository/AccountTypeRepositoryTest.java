package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.AccountTypeModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class AccountTypeRepositoryTest {
    @Autowired
    private AccountTypeRepository accountTypeRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getAccountTypeModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        AccountTypeModel expected = testData.getAccountTypeModels().get(0);
        AccountTypeModel actual = accountTypeRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(accountTypeRepository.findById(amountOfRows+1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<AccountTypeModel> expected = testData.getAccountTypeModels();
        List<AccountTypeModel> actual = accountTypeRepository.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(accountTypeRepository.findById(amountOfRows).isPresent());
        accountTypeRepository.deleteById(amountOfRows);
        assertFalse(accountTypeRepository.findById(amountOfRows).isPresent());
    }

    @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
        Assertions.assertThrows(EmptyResultDataAccessException.class,
                () -> accountTypeRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        AccountTypeModel accountTypeModel = new AccountTypeModel(null, "new account type");
        assertFalse(accountTypeRepository.findById(amountOfRows + 1).isPresent());
        accountTypeRepository.save(accountTypeModel);
        assertEquals(accountTypeModel, accountTypeRepository.findById(accountTypeModel.getId()).get());
        assertEquals(amountOfRows +1, accountTypeRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        AccountTypeModel accountTypeModel = testData.getAccountTypeModels().get(0);
        accountTypeModel.setName("CASH_test");
        assertNotEquals(accountTypeModel, accountTypeRepository.findById(accountTypeModel.getId()).get());
        accountTypeRepository.save(accountTypeModel);
        assertEquals(accountTypeModel, accountTypeRepository.findById(accountTypeModel.getId()).get());
        assertEquals(amountOfRows, accountTypeRepository.findAll().size());
    }
}
