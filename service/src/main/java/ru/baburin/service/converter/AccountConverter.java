package ru.baburin.service.converter;

import ru.baburin.dao.model.AccountModel;
import ru.baburin.service.model.Account;

public class AccountConverter {
    public AccountModel toAccountModel(Account account){
        AccountModel accountModel = new AccountModel();
        accountModel.setId(account.getId());
        accountModel.setName(account.getName());
        accountModel.setAmount(account.getAmount());
        accountModel.setCurrency(new CurrencyConverter().toCurrencyModel(account.getCurrency()));
        accountModel.setRegistrationDate(account.getRegistrationDate());
        accountModel.setType(new AccountTypeConverter().toAccountTypeModel(account.getType()));
        return accountModel;
    }

    public Account toAccount(AccountModel accountModel){
        Account account = new Account();
        account.setId(accountModel.getId());
        account.setName(accountModel.getName());
        account.setAmount(accountModel.getAmount());
        account.setCurrency(new CurrencyConverter().toCurrency(accountModel.getCurrency()));
        account.setRegistrationDate(accountModel.getRegistrationDate());
        account.setType(new AccountTypeConverter().toAccountType(accountModel.getType()));
        return account;
    }
}
