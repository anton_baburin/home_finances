package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.Currency;
import ru.baburin.service.model.CurrencyChangeHistory;
import ru.baburin.service.service.CurrencyChangeHistoryService;
import ru.baburin.service.service.CurrencyService;

import java.util.List;

@Controller
@RequestMapping("admin/currency_change_history")
public class CurrencyChangeHisatoryController {

    @Autowired
    private CurrencyService currencyService;
    @Autowired
    private CurrencyChangeHistoryService currencyChangeHistoryService;

    @GetMapping("/find_all")
    public String findAll(Model model){
        List<CurrencyChangeHistory> currencyChangeHistories = currencyChangeHistoryService.findAll();
        model.addAttribute("currencyChangeHistories", currencyChangeHistories);
        return "currency_change_history/find_all";
    }

    @GetMapping("/fill_save_form")
    public String fillSaveForm(Model model){
        CurrencyChangeHistory currencyChangeHistory = new CurrencyChangeHistory();
        model.addAttribute("currencyChangeHistory", currencyChangeHistory);
        return "currency_change_history/currency_change_history_form";
    }

    @GetMapping("/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        CurrencyChangeHistory currencyChangeHistory = currencyChangeHistoryService.findById(id);
        model.addAttribute("currencyChangeHistory", currencyChangeHistory);
        return "currency_change_history/currency_change_history_form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("currencyChangeHistory") CurrencyChangeHistory currencyChangeHistory) {
        currencyChangeHistory.setCurrency(currencyService.findById(currencyChangeHistory.getCurrency().getId()));
        currencyChangeHistoryService.save(currencyChangeHistory);
        return "redirect:/admin/currency_change_history/find_all";
    }

    @GetMapping(value="/delete")
    public String remove(@RequestParam("id") Long id) {
        currencyChangeHistoryService.deleteById(Long.valueOf(id));
        return "redirect:/admin/currency_change_history/find_all";
    }

    @ModelAttribute("currencies")
    public List<Currency> getCurrencies(){
        return currencyService.findAll();
    }
}
