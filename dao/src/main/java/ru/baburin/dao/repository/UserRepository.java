package ru.baburin.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
    UserModel findByUsername(String username);
}
