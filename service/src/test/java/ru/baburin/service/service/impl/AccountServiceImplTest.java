package ru.baburin.service.service.impl;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.AccountRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.Account;
import ru.baburin.service.service.AccountService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {ServiceConfiguration.class})
public class AccountServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private AccountRepository accountRepository;

    @Autowired
    @InjectMocks
    private AccountService accountService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(accountRepository.findAll()).thenReturn(testDataDao.getAccountModels());
        List<Account> actual = testDataService.getAccounts();
        List<Account> expected = accountService.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(accountRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getAccountModels().get(0)));
        Account actual = testDataService.getAccounts().get(0);
        Account expected = accountService.findById(1l);
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        Account account = testDataService.getAccounts().get(0);
        account.setId(null);
        accountService.save(account);
        verify(accountRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        Account account = testDataService.getAccounts().get(0);
        account.setAmount(new BigDecimal("111.0"));
        accountService.save(account);
        verify(accountRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        accountService.deleteById(3l);
        verify(accountRepository).deleteById(anyLong());
    }


}
