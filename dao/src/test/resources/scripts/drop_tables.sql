SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS transaction_tbl;
DROP TABLE IF EXISTS category_transaction_tbl;
DROP TABLE IF EXISTS currency_change_history_tbl;
DROP TABLE IF EXISTS account_tbl;
DROP TABLE IF EXISTS currency_tbl;
DROP TABLE IF EXISTS account_type_tbl;
DROP TABLE IF EXISTS user_tbl;
SET FOREIGN_KEY_CHECKS=1;
