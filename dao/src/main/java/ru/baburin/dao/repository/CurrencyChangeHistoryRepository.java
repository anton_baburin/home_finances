package ru.baburin.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.CurrencyChangeHistoryModel;
import ru.baburin.dao.model.CurrencyModel;

import java.time.LocalDate;
import java.util.List;

public interface CurrencyChangeHistoryRepository extends JpaRepository<CurrencyChangeHistoryModel, Long> {
    List<CurrencyChangeHistoryModel> findByCurrencyAndChangeDateBetween
            (CurrencyModel currencyModel, LocalDate dateFrom, LocalDate dateTo);
}
