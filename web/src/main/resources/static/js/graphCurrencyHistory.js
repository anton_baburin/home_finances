var prepareChartContext = function (chartData) {
    var charParametrs = {
        labels: chartData.labels,
        datasets: [
            {
                fillColor: "rgba(172,194,132,0.4)",
                strokeColor: "#ACC26D",
                pointColor: "#fff",
                pointStrokeColor: "#9DB86D",
                data: chartData.dataset,
            }
        ]
    }
    return charParametrs;
};

var getChartData = function () {
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    var endpoint = "/extractData/changeHistoryDataWeek?id=" + id;
    $.getJSON(endpoint)
        .done(function (data) {
            var chartData = prepareChartContext(data[0]);
            var ctx = document.getElementById('chart').getContext('2d');
            new Chart(ctx).Line(chartData);

        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
};

getChartData();