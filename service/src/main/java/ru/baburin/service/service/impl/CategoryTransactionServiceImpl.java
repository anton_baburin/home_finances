package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.CategoryTransactionModel;
import ru.baburin.dao.repository.CategoryTransactionRepository;
import ru.baburin.service.converter.CategoryTransactionConverter;
import ru.baburin.service.model.CategoryTransaction;
import ru.baburin.service.service.CategoryTransactionService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryTransactionServiceImpl  implements CategoryTransactionService {

    @Autowired
    private CategoryTransactionRepository categoryTransactionRepository;
    private CategoryTransactionConverter categoryTransactionConverter = new CategoryTransactionConverter();

    @Override
    public void save(CategoryTransaction model) {
        categoryTransactionRepository.save(categoryTransactionConverter.toCategoryTransactionModel(model));
    }

    @Override
    public CategoryTransaction findById(Long id) {
        return categoryTransactionConverter.toCategoryTransaction(categoryTransactionRepository.findById(id).get());
    }

    @Override
    public List<CategoryTransaction> findAll() {
        List<CategoryTransaction> categoryTransactions = new ArrayList<>();
        for (CategoryTransactionModel model : categoryTransactionRepository.findAll()) {
            categoryTransactions.add(categoryTransactionConverter.toCategoryTransaction(model));
        }
        return categoryTransactions;
    }

    @Override
    public void deleteById(Long id) {
        categoryTransactionRepository.deleteById(id);
    }
}
