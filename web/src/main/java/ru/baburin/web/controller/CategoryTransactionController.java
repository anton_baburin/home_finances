package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.CategoryTransaction;
import ru.baburin.service.service.CategoryTransactionService;

import java.util.List;

@Controller
@RequestMapping("admin/category_transaction")
public class CategoryTransactionController {

    @Autowired
    private CategoryTransactionService categoryTransactionService;

    @GetMapping("/find_all")
    public String findAll(Model model){
        List<CategoryTransaction> categoryTransactions = categoryTransactionService.findAll();
        model.addAttribute("categoryTransactions", categoryTransactions);
        return "category_transaction/find_all";
    }

    @GetMapping("/fill_save_form")
    public String fillSaveForm(Model model){
        CategoryTransaction categoryTransaction = new CategoryTransaction();
        model.addAttribute("categoryTransaction", categoryTransaction);
        return "category_transaction/category_transaction_form";
    }

    @GetMapping("/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        CategoryTransaction categoryTransaction = categoryTransactionService.findById(id);
        model.addAttribute("categoryTransaction", categoryTransaction);
        return "category_transaction/category_transaction_form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("categoryTransaction") CategoryTransaction categoryTransaction) {
        categoryTransactionService.save(categoryTransaction);
        return "redirect:/admin/category_transaction/find_all";
    }

    @GetMapping(value="/delete")
    public String remove(@RequestParam("id") Long id) {
        categoryTransactionService.deleteById(Long.valueOf(id));
        return "redirect:/admin/category_transaction/find_all";
    }
}
