package ru.baburin.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "currency_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurrencyModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "currency_id")
    private Long id;

    private String name;

    private String symbol;

    @Column(name = "course_to")
    private BigDecimal courseTo;

    @ManyToOne
    @JoinColumn(name = "currency_to_id")
    private CurrencyModel currencyTo;

    public CurrencyModel(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyModel that = (CurrencyModel) o;
        return id.equals(that.id);
    }

    @Override
    public String toString() {
        return "CurrencyModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                ", courseTo=" + courseTo +
                ", currencyTo.id =" + currencyTo.id +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
