package ru.baburin.service.service;

import ru.baburin.service.model.User;

import java.util.List;

public interface UserService {
    void save(User model);
    User findById(Long id);
    List<User> findAll();
    void deleteById(Long id);
    User findByUsername(String username);
}
