package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.AccountType;
import ru.baburin.service.service.AccountTypeService;

import java.util.List;

@Controller
@RequestMapping("admin/account_type")
public class AccountTypeController {

    @Autowired
    private AccountTypeService accountTypeService;

    @GetMapping("/find_all")
    public String findAll(Model model){
        List<AccountType> accountTypes = accountTypeService.findAll();
        model.addAttribute("accountTypes", accountTypes);
        return "account_type/find_all";
    }

    @GetMapping("/fill_save_form")
    public String fillSaveForm(Model model){
        AccountType accountType = new AccountType();
        model.addAttribute("accountType", accountType);
        return "account_type/account_type_form";
    }

    @GetMapping("/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        AccountType accountType = accountTypeService.findById(id);
        model.addAttribute("accountType", accountType);
        return "account_type/account_type_form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("accountType") AccountType accountType) {
        accountTypeService.save(accountType);
        return "redirect:/admin/account_type/find_all";
    }

    @GetMapping(value="/delete")
    public String remove(@RequestParam("id") Long id) {
        accountTypeService.deleteById(Long.valueOf(id));
        return "redirect:/admin/account_type/find_all";
    }
}
