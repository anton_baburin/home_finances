package ru.baburin.service.service;

import ru.baburin.service.model.Transaction;

import java.util.List;

public interface TransactionService {
    void save(Transaction model);
    Transaction findById(Long id);
    List<Transaction> findAll();
    void deleteById(Long id);
}
