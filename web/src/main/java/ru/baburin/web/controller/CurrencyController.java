package ru.baburin.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.baburin.service.model.Currency;
import ru.baburin.service.service.CurrencyService;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("currency")
public class CurrencyController {

    @Autowired
    private CurrencyService currencyService;

    @GetMapping("/find_all")
    public String findAll(Model model) {
        List<Currency> currencies = currencyService.findAll();
        for (int i = 0; i < currencies.size(); i++) {
            Currency currency = currencies.get(i);
            currency.setCourseTo(
                    new BigDecimal(currency.getCourseTo()
                            .setScale(4, BigDecimal.ROUND_HALF_UP).stripTrailingZeros().toPlainString()));
            currencies.set(i, currency);
        }
        model.addAttribute("currencies", currencies);
        return "currency/find_all";
    }

    @GetMapping("/admin/fill_save_form")
    public String fillSaveForm(Model model) {
        Currency currency = new Currency();
        model.addAttribute("currency", currency);
        return "currency/currency_form";
    }

    @GetMapping("/admin/fill_update_form")
    public String fillUpdateForm(@RequestParam("id") Long id, Model model) {
        Currency currency = currencyService.findById(id);
        model.addAttribute("currency", currency);
        return "currency/currency_form";
    }

    @PostMapping("/admin/save")
    public String save(@ModelAttribute("currency") Currency currency) {
        currencyService.save(currency);
        return "redirect:/currency/find_all";
    }

    @GetMapping(value = "/admin/delete")
    public String remove(@RequestParam("id") Long id) {
        currencyService.deleteById(Long.valueOf(id));
        return "redirect:/currency/find_all";
    }

    @GetMapping(value = "/setMainCurrency")
    public String setMainCurrency(@RequestParam("id") Long id) {
        Currency currency = currencyService.findById(id);
        currencyService.setMainCurrency(currency);
        return "redirect:/currency/find_all";
    }

    @GetMapping(value = "/graph")
    public String drawGraph(@RequestParam("id") Long id, Model model) {
        // currencyChangeHistoryService.deleteById(Long.valueOf(id));
        Currency currency = currencyService.findById(id);
        Currency mainCurrency = currencyService.findById(currency.getCurrencyTo().getId());
        model.addAttribute("currency", currency);
        model.addAttribute("mainCurrency", mainCurrency);
        return "currency/graph";
    }


    @ModelAttribute("myCurrencies")
    public List<Currency> getCurrencies() {
        List<Currency> currencies = currencyService.findAll();
        return currencies;
    }

    @ModelAttribute("mainCurrency")
    public Currency getMainCurrency() {
        return currencyService.findById(1L).getCurrencyTo();
    }
}
