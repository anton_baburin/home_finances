package ru.baburin.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.Validator;
import ru.baburin.dao.config.DaoConfiguration;
import ru.baburin.service.service.*;
import ru.baburin.service.service.impl.*;
import ru.baburin.service.validator.UserValidator;

@Configuration
@Import(DaoConfiguration.class)
public class ServiceConfiguration {

    @Bean("accountTypeService")
    public AccountTypeService accountTypeService() {
        return new AccountTypeServiceImpl();
    }

    @Bean("currencyService")
    public CurrencyService currencyService() {
        return new CurrencyServiceImpl();
    }

    @Bean("accountService")
    public AccountService accountService() {
        return new AccountServiceImpl();
    }

    @Bean("currencyChangeHistoryService")
    public CurrencyChangeHistoryService currencyChangeHistoryService() {
        return new CurrencyChangeHistoryServiceImpl();
    }

    @Bean("categoryTransactionService")
    public CategoryTransactionService categoryTransactionService() {
        return new CategoryTransactionServiceImpl();
    }

    @Bean("transactionService")
    public TransactionService transactionService() {
        return new TransactionServiceImpl();
    }

    @Bean("userService")
    public UserService userService() {
        return new UserServiceImpl();
    }

    @Bean("userDetailsService")
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean("userValidator")
    public Validator userValidator() {
        return new UserValidator();
    }
}
