package ru.baburin.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.AccountTypeModel;

public interface AccountTypeRepository extends JpaRepository<AccountTypeModel, Long> {
}
