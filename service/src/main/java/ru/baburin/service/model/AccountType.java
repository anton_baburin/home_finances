package ru.baburin.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountType {
    Long id;
    String name;
    //CASH, DEBIT_CARD, CREDIT_CARD, DEPOSIT;

    public AccountType(Long id) {
        this.id = id;
    }
}