# **Home Finances** #
#### Home finances is for private use to help collect data about your transactions and accounts
---
#### **Functionality:** ####
* create/edit/delete data about transactions
* create/edit/delete category of transaction
* create/edit/delete data about account
* create/edit/delete data about type of account
* create/edit/delete data about currency
* change main currency regarding which all transactions are calculated
* create/edit/delete data about data on how the exchange rate has changed
* currency rate graph for the last week
* authorization and user registration
* sharing user access to pages depending on their rights access rights
#### **Tools:** ####
* JDK 8
* Spring 5 (Core, MVC, Security)
* JPA / Hibernate
* MySql, H2Database
* JUnit 5, Mockito
* Thymeleaf
* Log4j
* Maven 3
* jQuery, Bootstrap, CSS
* Tomcat 8
* Git / Bitbucket
* IntelliJIDEA 2019.1

---
#### **Babrin Anton** ####
Тренинг GeekFactory,  
http://www.geekfactory.ru
![starting_page.jpg](images/starting_page.png)
![all_accounts.jpg](images/all_accounts.png)
![edit_account.jpg](images/edit_account.png)
![graph.jpg](images/graph.png)