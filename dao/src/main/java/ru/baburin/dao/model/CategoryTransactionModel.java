package ru.baburin.dao.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "category_transaction_tbl")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryTransactionModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_transaction_id")
    private Long id;

    private String name;
}
