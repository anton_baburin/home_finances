package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.Role;
import ru.baburin.dao.model.UserModel;
import ru.baburin.dao.repository.UserRepository;
import ru.baburin.service.converter.UserConverter;
import ru.baburin.service.model.User;
import ru.baburin.service.service.UserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    private UserConverter userConverter = new UserConverter();
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User model) {
        model.setPassword(bCryptPasswordEncoder.encode(model.getPassword()));
        model.setRole(Role.USER);
        userRepository.save(userConverter.toUserModel(model));
    }

    @Override
    public User findById(Long id) {
        return userConverter.toUser(userRepository.findById(id).get());
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        for (UserModel model : userRepository.findAll()) {
            users.add(userConverter.toUser(model));
        }
        return users;
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User findByUsername(String username) {
        UserModel userModel = userRepository.findByUsername(username);
        return userModel != null ? userConverter.toUser(userModel) : null;
    }
}
