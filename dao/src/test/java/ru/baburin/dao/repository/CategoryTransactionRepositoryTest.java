package ru.baburin.dao.repository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.config.DaoTestConfiguration;
import ru.baburin.dao.model.CategoryTransactionModel;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {DaoTestConfiguration.class})
public class CategoryTransactionRepositoryTest {
    @Autowired
    private CategoryTransactionRepository categoryTransactionRepository;
    @Autowired
    private InitializingOperations initializingOperations;
    private TestData testData = new TestData();
    private long amountOfRows = testData.getAccountModels().size();

    @BeforeEach
    public void beforeEach() {
        initializingOperations.fillTables();
    }

    @AfterEach
    public void afterEach() {
        initializingOperations.clearTables();
    }

    @Test
    @DisplayName("Test findById if id exists in DB")
    public void findByIdSuccessTest() {
        CategoryTransactionModel expected = testData.getCategoryTransactionModels().get(0);
        CategoryTransactionModel actual = categoryTransactionRepository.findById(1l).get();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test findById if id not exists in DB")
    public void findByIdFailTest() {
        assertFalse(categoryTransactionRepository.findById(amountOfRows + 1).isPresent());
    }

    @Test
    @DisplayName("Test findAll amount of founded rows")
    public void findAllSuccessTest() {
        List<CategoryTransactionModel> expected = testData.getCategoryTransactionModels();
        List<CategoryTransactionModel> actual = categoryTransactionRepository.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test deleteById if id exists in DB")
    public void deleteByIdSuccessTest() {
        assertTrue(categoryTransactionRepository.findById(amountOfRows).isPresent());
        categoryTransactionRepository.deleteById(amountOfRows);
        assertFalse(categoryTransactionRepository.findById(amountOfRows).isPresent());
    }

     @Test
    @DisplayName("Test deleteById if id not exists in DB")
    public void deleteByIdFailTest() {
         Assertions.assertThrows(EmptyResultDataAccessException.class,
                 () -> categoryTransactionRepository.deleteById(amountOfRows + 1));
    }

    @Test
    @DisplayName("Test save if id doesn't exist in DB")
    public void saveSuccessTest() {
        CategoryTransactionModel categoryTransactionModel = new CategoryTransactionModel(null, "cat " + amountOfRows);
        assertFalse(categoryTransactionRepository.findById(amountOfRows + 1).isPresent());
        categoryTransactionRepository.save(categoryTransactionModel);
        assertEquals(categoryTransactionModel, categoryTransactionRepository.findById(categoryTransactionModel.getId()).get());
        assertEquals(amountOfRows + 1, categoryTransactionRepository.findAll().size());
    }

    @Test
    @DisplayName("Test updateByID if id exists in DB")
    public void updateSuccessTest() {
        CategoryTransactionModel categoryTransactionModel = testData.getCategoryTransactionModels().get(0);
        categoryTransactionModel.setName("category 1.1");
        assertNotEquals(categoryTransactionModel, categoryTransactionRepository.findById(categoryTransactionModel.getId()).get());
        categoryTransactionRepository.save(categoryTransactionModel);
        assertEquals(categoryTransactionModel, categoryTransactionRepository.findById(categoryTransactionModel.getId()).get());
        assertEquals(amountOfRows, categoryTransactionRepository.findAll().size());
    }
}
