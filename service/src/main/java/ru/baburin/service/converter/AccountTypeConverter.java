package ru.baburin.service.converter;

import ru.baburin.dao.model.AccountTypeModel;
import ru.baburin.service.model.AccountType;

public class AccountTypeConverter {
    public AccountType toAccountType(AccountTypeModel accountTypeModel){
        AccountType accountType = new AccountType();
        accountType.setId(accountTypeModel.getId());
        accountType.setName(accountTypeModel.getName());
        return accountType;
    }

    public AccountTypeModel toAccountTypeModel(AccountType accountType){
        AccountTypeModel accountTypeModel = new AccountTypeModel();
        accountTypeModel.setId(accountType.getId());
        accountTypeModel.setName(accountType.getName());
        return accountTypeModel;
    }
}
