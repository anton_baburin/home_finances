package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.CategoryTransactionRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.CategoryTransaction;
import ru.baburin.service.service.CategoryTransactionService;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class CategoryTransactionServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private CategoryTransactionRepository categoryTransactionRepository;

    @Autowired
    @InjectMocks
    private CategoryTransactionService categoryTransactionService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(categoryTransactionRepository.findAll()).thenReturn(testDataDao.getCategoryTransactionModels());
        List<CategoryTransaction> actual = testDataService.getCategoryTransactions();
        List<CategoryTransaction> expected = categoryTransactionService.findAll();
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(categoryTransactionRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getCategoryTransactionModels().get(0)));
        CategoryTransaction actual = testDataService.getCategoryTransactions().get(0);
        CategoryTransaction expected = categoryTransactionService.findById(1l);
        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        CategoryTransaction categoryTransaction = testDataService.getCategoryTransactions().get(0);
        categoryTransaction.setId(null);
        categoryTransactionService.save(categoryTransaction);
        verify(categoryTransactionRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        CategoryTransaction categoryTransaction = testDataService.getCategoryTransactions().get(0);
        categoryTransaction.setName("new name");
        categoryTransactionService.save(categoryTransaction);
        verify(categoryTransactionRepository).save(anyObject());
    }

    @Test
    @Description("Test remove")
    public void removeTest() {
        categoryTransactionService.deleteById(3l);
        verify(categoryTransactionRepository).deleteById(anyLong());
    }
}
