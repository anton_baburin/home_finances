package ru.baburin.dao.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.CategoryTransactionModel;

public interface CategoryTransactionRepository extends JpaRepository<CategoryTransactionModel, Long> {
}
