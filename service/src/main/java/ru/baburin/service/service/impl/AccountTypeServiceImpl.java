package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.AccountTypeModel;
import ru.baburin.dao.repository.AccountTypeRepository;
import ru.baburin.service.converter.AccountTypeConverter;
import ru.baburin.service.model.AccountType;
import ru.baburin.service.service.AccountTypeService;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountTypeServiceImpl implements AccountTypeService {

    @Autowired
    private AccountTypeRepository accountTypeRepository;
    private AccountTypeConverter accountTypeConverter = new AccountTypeConverter();

    @Override
    public void save(AccountType model) {
        accountTypeRepository.save(accountTypeConverter.toAccountTypeModel(model));
    }

    @Override
    public AccountType findById(Long id) {
        return accountTypeConverter.toAccountType(accountTypeRepository.findById(id).get());
    }

    @Override
    public List<AccountType> findAll() {
        List<AccountType> accountTypes = new ArrayList<>();
        for (AccountTypeModel model : accountTypeRepository.findAll()) {
            accountTypes.add(accountTypeConverter.toAccountType(model));
        }
        return accountTypes;
    }

    @Override
    public void deleteById(Long id) {
        accountTypeRepository.deleteById(id);
    }
}
