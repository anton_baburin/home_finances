package ru.baburin.dao.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import ru.baburin.dao.model.TransactionModel;

public interface TransactionRepository extends JpaRepository<TransactionModel, Long> {
}
