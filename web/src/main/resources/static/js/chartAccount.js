var drawPieChart = function (chartData) {
    var ctx = document.getElementById('chart');
    new Chart(ctx, {
        type: 'pie',
        data: {
            labels: chartData.labels,
            datasets: [{
                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
                data: chartData.dataset,
            }]
        }
    });
};

var getChartData = function () {
    var url = new URL(window.location.href);
    var id = url.searchParams.get("id");
    var endpoint = "/extractData/accountsData?id=" + id;
    $.getJSON(endpoint)
        .done(function (data) {
            drawPieChart(data[0]);
        })
        .fail(function (jqxhr, textStatus, error) {
            var err = textStatus + ", " + error;
            console.log("Request Failed: " + err);
        });
};

getChartData();