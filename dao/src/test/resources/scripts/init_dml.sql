INSERT INTO user_tbl (user_id, username, password, role)
VALUES (1, 'username 1', '12345678', 'ADMIN');
INSERT INTO user_tbl (user_id, username, password, role)
VALUES (2, 'username 2', '12345678', 'USER');
INSERT INTO user_tbl (user_id, username, password, role)
VALUES (3, 'username 3', '12345678', 'USER');

INSERT INTO account_type_tbl (account_type_id, name) VALUES (1, 'Cash');
INSERT INTO account_type_tbl (account_type_id, name) VALUES (2, 'Debit');
INSERT INTO account_type_tbl (account_type_id, name) VALUES (3, 'Deposit');

SET FOREIGN_KEY_CHECKS=0;
INSERT INTO currency_tbl (currency_id, name, symbol, course_to, currency_to_id) VALUES (1, 'USD', '$', 1, 1);
SET FOREIGN_KEY_CHECKS=1;
INSERT INTO currency_tbl (currency_id, name, symbol, course_to, currency_to_id) VALUES (2, 'RUB', 'R', 60, 1);
INSERT INTO currency_tbl (currency_id, name, symbol, course_to, currency_to_id) VALUES (3, 'EUR', 'E', 0.8, 1);

INSERT INTO account_tbl (account_id, user_id, name, amount, registration_date, account_type_id, currency_id)
VALUES (1, 1, 'Main', 50000, '2018-10-10', 2, 1);
INSERT INTO account_tbl (account_id, user_id, name, amount, registration_date, account_type_id, currency_id)
VALUES (2, 1, 'Emergency', 60000, '2019-01-01', 1, 2);
INSERT INTO account_tbl (account_id, user_id, name, amount, registration_date, account_type_id, currency_id)
VALUES (3, 2, 'Salary card', 100000, '2017-02-01', 1, 1);


INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (1, 2, '2018-12-12', 58, 1);
INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (2, 2, '2019-01-01', 62, 1);
INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (3, 2, '2017-02-03', 0.9, 1);

INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (100, 2, '2019-08-18', 58, 1);
INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (102, 2, '2019-08-20', 62, 1);
INSERT INTO currency_change_history_tbl (currency_change_history_id, currency_id, change_date, course_to, currency_to_id)
VALUES (103, 2, '2019-08-23', 56, 1);

INSERT INTO category_transaction_tbl (category_transaction_id, name) VALUES (1, 'category 1');
INSERT INTO category_transaction_tbl (category_transaction_id, name) VALUES (2, 'category 2');
INSERT INTO category_transaction_tbl (category_transaction_id, name) VALUES (3, 'category 3');



INSERT INTO transaction_tbl (transaction_id, sender_id, receiver_id, transaction_date, amount, category_transaction_id)
VALUES(1, 1, 2, '2018-10-10', 300, 1);
INSERT INTO transaction_tbl (transaction_id, sender_id, receiver_id, transaction_date, amount, category_transaction_id)
VALUES(2, 1, 2, '2017-09-10', 600, 1);
INSERT INTO transaction_tbl (transaction_id, sender_id, receiver_id, transaction_date, amount, category_transaction_id)
VALUES(3, 2, 1, '2015-10-03', 400, 2);
