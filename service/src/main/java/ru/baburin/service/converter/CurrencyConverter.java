package ru.baburin.service.converter;

import ru.baburin.dao.model.CurrencyModel;
import ru.baburin.service.model.Currency;

public class CurrencyConverter {
    public Currency toCurrency(CurrencyModel currencyModel){
        Currency currency = new Currency();
        currency.setId(currencyModel.getId());
        currency.setCourseTo(currencyModel.getCourseTo());
        currency.setName(currencyModel.getName());
        currency.setSymbol(currencyModel.getSymbol());
        currency.setCurrencyTo(new Currency(currencyModel.getCurrencyTo().getId()));
        return currency;
    }

    public CurrencyModel toCurrencyModel(Currency currency){
        CurrencyModel currencyModel = new CurrencyModel();
        currencyModel.setId(currency.getId());
        currencyModel.setCourseTo(currency.getCourseTo());
        currencyModel.setName(currency.getName());
        currencyModel.setSymbol(currency.getSymbol());
        currencyModel.setCurrencyTo(new CurrencyModel(currency.getCurrencyTo().getId()));
        return currencyModel;
    }
}
