package ru.baburin.service.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.baburin.dao.repository.TransactionRepository;
import ru.baburin.service.TestDataDao;
import ru.baburin.service.TestDataService;
import ru.baburin.service.config.ServiceConfiguration;
import ru.baburin.service.model.Transaction;
import ru.baburin.service.service.TransactionService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfiguration.class)
public class TransactionServiceImplTest {
    private TestDataDao testDataDao = new TestDataDao();
    private TestDataService testDataService = new TestDataService();

    @Mock
    private TransactionRepository transactionRepository;

    @Autowired
    @InjectMocks
    private TransactionService transactionService;

    @BeforeEach
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @Description("Test findAll")
    public void findAllTest() {
        when(transactionRepository.findAll()).thenReturn(testDataDao.getTransactionModels());
        List<Transaction> actual = testDataService.getTransactions();
        List<Transaction> expected = transactionService.findAll();

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test findByID")
    public void findByIdTest() {
        when(transactionRepository.findById(1l)).thenReturn(
                Optional.of(testDataDao.getTransactionModels().get(0)));
        Transaction actual = testDataService.getTransactions().get(0);
        Transaction expected = transactionService.findById(1l);

        assertEquals(expected, actual);
    }

    @Test
    @Description("Test save")
    public void saveTest() {
        Transaction transaction = testDataService.getTransactions().get(0);
        transaction.setId(null);
        transactionService.save(transaction);
        verify(transactionRepository).save(anyObject());
    }

    @Test
    @Description("Test updateById")
    public void updateByIdTest() {
        Transaction transaction = testDataService.getTransactions().get(0);
        transaction.setAmount(new BigDecimal("10.9"));
        transactionService.save(transaction);
        verify(transactionRepository).save(anyObject());
    }

    @Test
    @Description("Test deleteById")
    public void deleteByIdTest() {
        transactionService.deleteById(3l);
        verify(transactionRepository).deleteById(anyLong());
    }
}

