package ru.baburin.service.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baburin.dao.model.CurrencyChangeHistoryModel;
import ru.baburin.dao.model.CurrencyModel;
import ru.baburin.dao.repository.CurrencyChangeHistoryRepository;
import ru.baburin.dao.repository.CurrencyRepository;
import ru.baburin.service.converter.CurrencyChangeHistoryConverter;
import ru.baburin.service.converter.CurrencyConverter;
import ru.baburin.service.model.Currency;
import ru.baburin.service.model.CurrencyChangeHistory;
import ru.baburin.service.service.CurrencyChangeHistoryService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class CurrencyChangeHistoryServiceImpl implements CurrencyChangeHistoryService {

    @Autowired
    private CurrencyChangeHistoryRepository currencyChangeHistoryRepository;

    @Autowired
    private CurrencyRepository currencyRepository;
    private CurrencyChangeHistoryConverter currencyChangeHistoryConverter = new CurrencyChangeHistoryConverter();
    private CurrencyConverter currencyConverter = new CurrencyConverter();

    @Override
    public void save(CurrencyChangeHistory model) {
        model.setChangeDate(LocalDate.now());
        CurrencyModel currencyTo = currencyRepository.findById( model.getCurrency().getCurrencyTo().getId()).get();
        model.setCurrencyTo(currencyConverter.toCurrency(currencyTo));
        currencyChangeHistoryRepository.save(currencyChangeHistoryConverter.toCurrencyChangeHistoryModel(model));
    }

    @Override
    public CurrencyChangeHistory findById(Long id) {
        return currencyChangeHistoryConverter.toCurrencyChangeHistory(currencyChangeHistoryRepository.findById(id).get());
    }

    @Override
    public List<CurrencyChangeHistory> findAll() {
        List<CurrencyChangeHistory> currencyChangeHistories = new ArrayList<>();
        for (CurrencyChangeHistoryModel model : currencyChangeHistoryRepository.findAll()) {
            currencyChangeHistories.add(currencyChangeHistoryConverter.toCurrencyChangeHistory(model));
        }
        return currencyChangeHistories;
    }

    @Override
    public void deleteById(Long id) {
        currencyChangeHistoryRepository.deleteById(id);
    }


    @Override
    public List<CurrencyChangeHistory> findByCurrencyAndDate(Currency currency, LocalDate dateFrom, LocalDate dateTo) {
        List<CurrencyChangeHistory> currencyChangeHistories = new ArrayList<>();
        for (CurrencyChangeHistoryModel model : currencyChangeHistoryRepository.findByCurrencyAndChangeDateBetween(currencyConverter.toCurrencyModel(currency), dateFrom, dateTo)) {
            currencyChangeHistories.add(currencyChangeHistoryConverter.toCurrencyChangeHistory(model));
        }
        return currencyChangeHistories;
    }
}
