package ru.baburin.service.converter;

import ru.baburin.dao.model.UserModel;
import ru.baburin.service.model.User;

public class UserConverter {
    public User toUser(UserModel userModel){
        User user = new User();
        user.setId(userModel.getId());
        user.setUsername(userModel.getUsername());
        user.setPassword(userModel.getPassword());
        user.setConfirmPassword(userModel.getConfirmPassword());
        user.setRole(userModel.getRole());
        return user;
}

    public UserModel toUserModel(User user){
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setUsername(user.getUsername());
        userModel.setPassword(user.getPassword());
        userModel.setConfirmPassword(user.getConfirmPassword());
        userModel.setRole(user.getRole());
        return userModel;
    }
}
